import tkinter as tk
from tkinter import ttk
              
root1 = tk.Tk()
root1.title("Building Bridges")
root1.geometry("600x400")
root1.configure(bg="lightblue")

label_text = "Welcome to Building Bridges!"
label = tk.Label(root1, text=label_text,font=("Arial",20),foreground="blue" , bg="lightyellow")
label.pack(padx=50,pady=50)

start_button = tk.Button(root1, text="Start", command=lambda:second_window())
start_button.pack()

num_cities = 0
def second_window():
    max_cities = 4
    def add_city():
        global num_cities
        if num_cities >= max_cities:
            add_city_button.config(state=tk.DISABLED)
            return
        rows, cols = map(int, rows_columns_entry.get().split())
        city_frame = ttk.Frame(input_frame)
        city_frame.pack(pady=5)
        city_label = ttk.Label(city_frame, text=f"City {len(city_entries) + 1}")
        city_label.pack()
        city_entry = []
        for i in range(rows):
            row_frame = ttk.Frame(city_frame)
            row_frame.pack()
            row_entry = []
            for j in range(cols):
                cell_entry = ttk.Entry(row_frame, width=3)
                cell_entry.pack(side=tk.LEFT)
                row_entry.append(cell_entry)
            city_entry.append(row_entry)

        city_entries.append(city_entry)
        num_cities += 1
    CHAR_TO_HEIGHT = {
        '#': 2,  
        '.': 0, 
    }
    def display_layout():
        for city_entry in city_entries:
            city_layout = []
            for row_entry in city_entry:
                row_layout = []
                for cell_entry in row_entry:
                    building_char = cell_entry.get()
                    building_height = CHAR_TO_HEIGHT.get(building_char, 0)
                    row_layout.append(building_height)
                city_layout.append(row_layout)

            city_window = tk.Toplevel(root)
            city_window.title("City layout")
            city_window.geometry("600x400")
            city_window.configure(padx=10,pady=20,bg="lightyellow")
            frame = tk.Frame(city_window,borderwidth=2,relief="solid")  
            frame.pack(padx=100, pady=100) 
            label2=tk.Label(city_window,bg="lightgrey",text="City layout without bridges",font=("Arial",30))
            label2.pack()
            for i in range(len(city_layout)):
                for j in range(len(city_layout[i])):
                    building_height = city_layout[i][j]
                    color = 'brown' if building_height > 0 else 'white'
                    label=tk.Label(frame,borderwidth=0.5,relief="solid", width=5,text='',background=color).grid(row=i, column=j)
                   
            
    root = tk.Tk()
    root.title("Building Bridges")
    root.geometry("600x400")
    root.configure(bg="lightblue")

    input_frame = tk.Frame(root)
    input_frame.configure(bg="blue")
    input_frame.pack()

    rows_columns_entry = ttk.Entry(input_frame)
    rows_columns_entry.pack(pady=20)

    city_entries = []

    add_city_button = ttk.Button(input_frame,text="Add City",command=lambda:add_city())                         
    add_city_button.pack(pady=5)
    display_button = ttk.Button(root, text="Display Layout", command=display_layout)
    display_button.pack(pady=10)

    root.mainloop()
root1.mainloop()
